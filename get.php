<meta charset="utf-8"> 

<?
include_once "config.php";

$email = $_POST['email'];
$name = $_POST['name'];
$msg = $_POST['message'];
$title = 'Вам пришло письмо с http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$chestionar = 'Вам понравилась данная форма?' . "\r\n" . $_POST['like'] . "\r\n" ;
$chestionar.= 'Удобно ли пользоваться данной формой?' . "\r\n" . $_POST['useful'] . "\r\n" ;
$chestionar.= 'Заметили ли вы меню выбора цвета сверху?' . "\r\n" . $_POST['color'] . "\r\n" ;
$chestionar.= 'Устраивает ли вас предоставленная цветовая политра?' . "\r\n" . $_POST['likecolor'] . "\r\n";

$color = $_POST['dva'];

if (empty($_POST['like']) && empty($_POST['useful']) && empty($_POST['color']) && empty($_POST['likecolor'])) {
    $letter = 'от: ' . $name . "\r\n" . 'сообщение: ' . $msg . "\r\n" . 'время открытия формы: ' . $_POST['info'] . "\r\n" . 'время отправки сообщения: ' . date("Y-m-d, H:i:s");
} else {
    $letter = 'от: ' . $name . "\r\n" . 'сообщение: ' . $msg . "\r\n" . $chestionar . "\r\n" . 'время открытия формы: ' . $_POST['info'] . "\r\n" . 'время отправки сообщения: ' . date("Y-m-d, H:i:s");
}

$headers  = "Content-type: text/plain; charset=utf-8";

$mail_result = mail($email, $title, $letter, $headers);

if ($mail_result == '1') {
    echo 'Сообщение отправлено';      
}

if ($_POST['edit'] !== 'on') {
    echo ' Пожалуйста, заполните в следующий раз анкету!';      
}

// Cоздание подключения к базе данных

$dsn = "mysql:host=$db_host;dbname=$db_base;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$dbh = new PDO($dsn, $db_user, $db_pass, $opt);


//Записываем в БД
$query_insert = 'INSERT INTO messages (name, message, email, form_time, message_time, w_like, useful, color, like_color, checkbox) VALUES ("' . $name . '", "' . $msg . '", "' . $email . '", "' . $_POST['info'] . '", "' . date("Y-m-d, H:i:s") . '", "' . $_POST['like'] . '", "' . $_POST['useful'] . '", "' . $_POST['color'] . '", "' . $_POST['likecolor'] . '", "' . $_POST['edit'] . '")';

$stm = $dbh->prepare($query_insert);

// Закрыть подключения к базе данных
$stm->execute();
 ?>